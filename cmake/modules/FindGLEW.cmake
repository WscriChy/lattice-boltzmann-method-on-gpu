find_path(GLEW_INCLUDE_DIR
          NAMES GL/glew.h
          PATHS ${GLEW_INCLUDE_DIR}
                $ENV{GLEW_INCLUDE_DIR})

find_library(GLEW_LIBRARY
             NAMES GLEW
                   glew
                   glew32
             PATHS ${GLEW_LIBRARY_DIR}
                   $ENV{GLEW_LIBRARY_DIR})

set(GLEW_INCLUDE_DIRS ${GLEW_INCLUDE_DIR})

set(GLEW_LIBRARIES ${GLEW_LIBRARY})

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(GLEW
                                  DEFAULT_MSG
                                  GLEW_INCLUDE_DIR
                                  GLEW_LIBRARY)

mark_as_advanced(GLEW_INCLUDE_DIR
                 GLEW_LIBRARY)
