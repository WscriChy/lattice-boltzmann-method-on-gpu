#ifndef Handedness_hpp
#define Handedness_hpp

enum class Handedness {
  right,
  left,
};

#endif