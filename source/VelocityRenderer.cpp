#include "VelocityRenderer.hpp"

#include "VectorField.hpp"

#include <oglplus/bound/buffer.hpp>

#include <QtCore/QDebug>

#include <vector>
// -----------------------------------------------------------------------------

using Math::Matrix4x4f;

using oglplus::Buffer;
using oglplus::Capability;
using oglplus::DataType;
using oglplus::FragmentShader;
using oglplus::PrimitiveType;
using oglplus::Program;
using oglplus::Uniform;
using oglplus::UniformSampler;
using oglplus::VertexArray;
using oglplus::VertexShader;

using std::vector;

namespace {
GLuint const vertexCount = 5 + 8;
GLuint const indexCount  = 3 * (5 * 2 + 2 + 4);
}

VelocityRenderer::
VelocityRenderer(VectorField const* velocityField):
  _velocityField(velocityField) {
  assert(sampleCountX() >= 1);
  assert(sampleCountY() >= 1);
  assert(sampleCountZ() >= 1);

  _instanceCount = sampleCountX() * sampleCountY() * sampleCountZ();

  initializeProgram();
  initializeGlyph();
}

GLuint
VelocityRenderer::
sampleCountX() const
{ return _velocityField->sampleCountX(); }

GLuint
VelocityRenderer::
sampleCountY() const
{ return _velocityField->sampleCountY(); }

GLuint
VelocityRenderer::
sampleCountZ() const
{ return _velocityField->sampleCountZ(); }

void
VelocityRenderer::
initializeProgram() {
  VertexShader   vs;
  FragmentShader fs;

  vs.Source(
    "#version 400\n"

    "uniform sampler3D _velocitySampler;"

    "uniform mat4 _mvp;"

    "uniform uvec3 _sampleCount;"

    "uniform float _scale;"

    "uniform vec3 _color = vec3(1, 1, 1);"

    "in vec3  _position;"
    "in float _shade;"

    "out vec3 v_color;"

    "vec4 "
    "rotationBetween(vec3 a, vec3 b) {"
    "  float d = dot(a, b);"
    "  float s = sqrt(dot(a, a) * dot(b, b));"

    "  if (d / s == -1)"
    "    return vec4(0, 0, 1, 0);"

    "  return normalize(vec4(cross(a, b), d + s));"
    "}"

    "vec3 "
    "rotate(vec4 q, vec3 v) {"
    "  return v + 2 * cross(q.xyz, cross(q.xyz, v) + q.w * v);"
    "}"

    "void "
    "main() {"
    "  uint x =  gl_InstanceID %  _sampleCount.x;"
    "  uint y =  gl_InstanceID / (_sampleCount.x * _sampleCount.z);"
    "  uint z = (gl_InstanceID % (_sampleCount.x * _sampleCount.z)) /"
    "            _sampleCount.x;"

    "  ivec3 translation = ivec3(x + 1, y + 1, z + 1);"

    "  vec3 velocity = texelFetch(_velocitySampler, translation, 0).rgb;"
    "  vec4 rotation = rotationBetween(vec3(0, 0, 1), velocity);"

    "  v_color = _shade * _color;"

    "  gl_Position = _mvp * vec4(_scale * length(velocity) * rotate(rotation, _position) + translation, 1);"
    "}").Compile();

  fs.Source(
    "#version 400\n"

    "in vec3 v_color;"

    "out vec4 f_color;"

    "void "
    "main() {"
    "  f_color = vec4(v_color, 1);"
    "}").Compile();

  _program.AttachShader(vs);
  _program.AttachShader(fs);

  _program.Link();

  _program.DetachShader(vs);
  _program.DetachShader(fs);

  _program.Use();

  UniformSampler(_program, "_velocitySampler").Set(0);
  Uniform<GLuint>(_program, "_sampleCount").SetVector(sampleCountX(),
                                                      sampleCountY(),
                                                      sampleCountZ());

  Program::UseNone();
}

void
VelocityRenderer::
initializeGlyph() {
  _vertexArray.Bind();
  {
    auto b = Bind(_positionBuffer, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(3 * vertexCount);

    data.push_back(0.05f);
    data.push_back(0.05f);
    data.push_back(0);

    data.push_back(-0.05f);
    data.push_back(0.05f);
    data.push_back(0);

    data.push_back(-0.05f);
    data.push_back(-0.05f);
    data.push_back(0);

    data.push_back(0.05f);
    data.push_back(-0.05f);
    data.push_back(0);

    data.push_back(0.05f);
    data.push_back(0.05f);
    data.push_back(0.8f);

    data.push_back(-0.05f);
    data.push_back(0.05f);
    data.push_back(0.8f);

    data.push_back(-0.05f);
    data.push_back(-0.05f);
    data.push_back(0.8f);

    data.push_back(0.05f);
    data.push_back(-0.05f);
    data.push_back(0.8f);

    data.push_back(0.1f);
    data.push_back(0.1f);
    data.push_back(0.8f);

    data.push_back(-0.1f);
    data.push_back(0.1f);
    data.push_back(0.8f);

    data.push_back(-0.1f);
    data.push_back(-0.1f);
    data.push_back(0.8f);

    data.push_back(0.1f);
    data.push_back(-0.1f);
    data.push_back(0.8f);

    data.push_back(0);
    data.push_back(0);
    data.push_back(1);

    b.Data(data);

    (_program | "_position").Setup<GLfloat>(3).Enable();
  }

  {
    auto b = Bind(_shadeBuffer, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(vertexCount);

    data.push_back(0.8f);
    data.push_back(0.6f);
    data.push_back(0.4f);
    data.push_back(0.6f);

    data.push_back(0.6f);
    data.push_back(0.4f);
    data.push_back(0.2f);
    data.push_back(0.4f);

    data.push_back(0.7f);
    data.push_back(0.5f);
    data.push_back(0.3f);
    data.push_back(0.5f);

    data.push_back(0.8f);

    b.Data(data);

    (_program | "_shade").Setup<GLfloat>(1).Enable();
  }

  {
    auto b = Bind(_indexBuffer, Buffer::Target::ElementArray);

    vector<GLuint> data;

    data.reserve(indexCount);

    data.push_back(0); data.push_back(2); data.push_back(1);
    data.push_back(0); data.push_back(3); data.push_back(2);

    data.push_back(8); data.push_back(10); data.push_back(9);
    data.push_back(8); data.push_back(11); data.push_back(10);

    data.push_back(0); data.push_back(5); data.push_back(4);
    data.push_back(0); data.push_back(1); data.push_back(5);

    data.push_back(1); data.push_back(6); data.push_back(5);
    data.push_back(1); data.push_back(2); data.push_back(6);

    data.push_back(2); data.push_back(7); data.push_back(6);
    data.push_back(2); data.push_back(3); data.push_back(7);

    data.push_back(3); data.push_back(4); data.push_back(7);
    data.push_back(3); data.push_back(0); data.push_back(4);

    data.push_back(8);  data.push_back(9);  data.push_back(12);
    data.push_back(9);  data.push_back(10); data.push_back(12);
    data.push_back(10); data.push_back(11); data.push_back(12);
    data.push_back(11); data.push_back(8);  data.push_back(12);

    b.Data(data);
  }

  VertexArray::Unbind();
}

void
VelocityRenderer::
render(Matrix4x4f const& mvp, GLfloat scale) const {
  auto cullFace  = _gl.IsEnabled(Capability::CullFace);
  auto depthTest = _gl.IsEnabled(Capability::DepthTest);

  _gl.Enable(Capability::CullFace);
  _gl.Enable(Capability::DepthTest);

  _velocityField->attach(0);

  _program.Use();

  Uniform<GLfloat>(_program, "_mvp").SetMatrix<4, 4>(1, mvp.data());
  Uniform<GLfloat>(_program, "_scale").Set(scale);

  _vertexArray.Bind();
  _gl.DrawElementsInstanced(PrimitiveType::Triangles,
                            indexCount,
                            DataType::UnsignedInt,
                            _instanceCount);
  VertexArray::Unbind();

  Program::UseNone();

  _velocityField->detach(0);

  if (!cullFace)
    _gl.Disable(Capability::CullFace);

  if (!depthTest)
    _gl.Disable(Capability::DepthTest);
}