#ifndef EulerRotationManager_hpp
#define EulerRotationManager_hpp

#include "Direction.hpp"
#include "Vector.hpp"
#include "sgn.hpp"

#include <QtCore/QDebug>

template <class T, Handedness handedness = Handedness::right>
class EulerRotationManager {
  using Direction = Math::Direction<T, handedness>;
  using Vector3   = Math::Vector3<T>;

public:
  EulerRotationManager(): _pitch(0), _yaw(0), _active(false)
  {}

  EulerRotationManager(EulerRotationManager const& other) = delete;

  T
  pitch() {
    auto p = _pitch;

    _pitch = 0;

    return p;
  }

  T
  yaw() {
    auto y = _yaw;

    _yaw = 0;

    return y;
  }

  void
  start() {
    if (_active)
      return;

    _active = true;
    _pitch  = 0;
    _yaw    = 0;
  }

  void
  advance(int x, int y, int width, int height, T fovY, T nearZ) {
    if (!_active)
      return;

    auto aspectRatio = T(width) / T(height);
    auto tanY        = std::tan(fovY / T(2));
    auto tanX        = tanY * aspectRatio;

    auto halfNearHeight = tanY * nearZ;
    auto halfNearWidth  = tanX * nearZ;

    auto nearY = halfNearHeight * (T(1) - 2 * T(y) / T(height));
    auto nearX = halfNearWidth *  (T(1) - 2 * T(x) / T(width));

    Vector3 from = Direction::forward();
    Vector3 toY  = nearZ * Direction::forward() +
                   nearY * Direction::up();

    Vector3 toX = nearZ * Direction::forward() +
                  nearX * Direction::left();

    _pitch -= Math::sgn(nearY) * std::acos(toY.normalized().dot(from));
    _yaw   += Math::sgn(nearX) * std::acos(toX.normalized().dot(from));
  }

  void
  stop() {
    if (!_active)
      return;

    _active = false;
  }

private:
  T    _pitch;
  T    _yaw;
  bool _active;
};

using EulerRotationManagerd = EulerRotationManager<double>;
using EulerRotationManagerf = EulerRotationManager<float>;

#endif