#include "VectorField.hpp"
// -----------------------------------------------------------------------------

using oglplus::PixelDataFormat;
using oglplus::PixelDataInternalFormat;
using oglplus::PixelDataType;
using oglplus::TextureMagFilter;
using oglplus::TextureMinFilter;

VectorField::
VectorField(GLuint sampleCountX,
            GLuint sampleCountY,
            GLuint sampleCountZ): Field(sampleCountX,
                                        sampleCountY,
                                        sampleCountZ,
                                        PixelDataInternalFormat::RGB32F,
                                        PixelDataFormat::RGB,
                                        PixelDataType::Float,
                                        TextureMinFilter::Linear,
                                        TextureMagFilter::Linear) {}
