#include "Application.hpp"

#include <GL/glew.h>

#include <oglplus/error.hpp>

#include <iostream>

int
main(int argc, char** argv) {
  Q_INIT_RESOURCE(resource);

  try {
    Application a(argc, argv);
    return a.exec();
  } catch (oglplus::Error& err) {
    std::cerr <<
      "Error (in " << err.GLSymbol() << ", " <<
      err.ClassName() << ": '" <<
      err.ObjectDescription() << "'): " <<
      err.what() <<
      " [" << err.File() << ":" << err.Line() << "] ";
    std::cerr << std::endl;
    err.Cleanup();
  }
}