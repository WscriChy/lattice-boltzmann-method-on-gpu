#ifndef DensityRenderer_hpp
#define DensityRenderer_hpp

#include "Matrix.hpp"

#include <GL/glew.h>

#include <oglplus/all.hpp>

class ScalarField;

class DensityRenderer {
public:
  DensityRenderer(ScalarField const* densityField);

  GLuint
  vertexCountX() const;

  GLuint
  vertexCountY() const;

  GLuint
  vertexCountZ() const;

  void
  render(Math::Matrix4x4f const& mvp, GLfloat alpha) const;

private:
  void
  initializeProgram();

  void
  initializeZY();

  void
  initializeXZ();

  void
  initializeXY();

  void
  renderZY() const;

  void
  renderXZ() const;

  void
  renderXY() const;

  oglplus::Context _gl;

  oglplus::Program _program;

  oglplus::VertexArray _vertexArrayZY;
  oglplus::VertexArray _vertexArrayXZ;
  oglplus::VertexArray _vertexArrayXY;

  oglplus::Buffer _vertexBufferZY;
  oglplus::Buffer _vertexBufferXZ;
  oglplus::Buffer _vertexBufferXY;

  oglplus::Buffer _indexBufferZY;
  oglplus::Buffer _indexBufferXZ;
  oglplus::Buffer _indexBufferXY;

  GLuint _subroutineZY;
  GLuint _subroutineXZ;
  GLuint _subroutineXY;

  GLuint _vertexCountZY;
  GLuint _vertexCountXZ;
  GLuint _vertexCountXY;

  GLuint _indexCountZY;
  GLuint _indexCountXZ;
  GLuint _indexCountXY;

  ScalarField const* _densityField;
};

#endif