#include "GLWidget.hpp"

#include "KeyboardEvent.hpp"
#include "MouseEvent.hpp"
#include "Scenario.hpp"

#include <QtCore/QDebug>

#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtOpenGL/QGLContext>

#include <cassert>
// -----------------------------------------------------------------------------

GLWidget::
GLWidget(ScenarioFactory::Shared scenarioFactory,
         QWidget*                parent): QGLWidget(parent),
                                          _scenarioFactory(scenarioFactory) {
  assert(scenarioFactory);

  setFocusPolicy(Qt::ClickFocus);
}

GLWidget::
~GLWidget() {
  if (_scenario)
    delete _scenario;
}

void
GLWidget::
initializeGL() {
  // GLEW obtains information on the supported extensions from the graphics
  // driver. Experimental or pre-release drivers, however, might not report
  // every available extension through the standard mechanism, in which case
  // GLEW will report it unsupported. To circumvent this situation, the
  // `glewExperimental` global switch can be turned on by setting it to
  // `GL_TRUE` before calling `glewInit`, which ensures that all extensions with
  // valid entry points will be exposed.
  glewExperimental = GL_TRUE;
  auto status = glewInit();

  if (status != GLEW_OK) {
    qFatal("Cannot initialize GLEW.");
    exit(1);
  }

  // NOTE: Apparently, `glewInit` call introduces `GL_INVALID_ENUM` (even on
  // success). This could lead subsequent code to misinterpret errors. For
  // instance, the OGLplus library would immediately crash as it offers
  // extensive // error handling via exceptions. As a result, the following call
  // is used to "eat" that stupid error.
  glGetError();

  _scenario = _scenarioFactory->createScenario(this);

  emit scenarioInitialized(_scenario);

  _scenario->initialize();
}

void
GLWidget::
resizeGL(int width, int height)
{ _scenario->resize(width, height); }

void
GLWidget::
paintGL()
{ _scenario->render(); }

void
GLWidget::
keyPressEvent(QKeyEvent* keyEvent) {
  KeyboardEvent event(static_cast<Key>(keyEvent->key()),
                      keyEvent->isAutoRepeat());

  static_cast<KeyboardListener*>(_scenario)->press(event);
}

void
GLWidget::
keyReleaseEvent(QKeyEvent* keyEvent) {
  KeyboardEvent event(static_cast<Key>(keyEvent->key()),
                      keyEvent->isAutoRepeat());

  static_cast<KeyboardListener*>(_scenario)->release(event);
}

void
GLWidget::
mousePressEvent(QMouseEvent* mouseEvent) {
  MouseEvent event(static_cast<Button>(mouseEvent->button()),
                   mouseEvent->x(),
                   mouseEvent->y(),
                   width(),
                   height(),
                   mouseEvent->globalX(),
                   mouseEvent->globalY(),
                   QApplication::desktop()->screenGeometry(this).width(),
                   QApplication::desktop()->screenGeometry(this).height());

  static_cast<MouseListener*>(_scenario)->press(event);
}

void
GLWidget::
mouseReleaseEvent(QMouseEvent* mouseEvent) {
  MouseEvent event(static_cast<Button>(mouseEvent->button()),
                   mouseEvent->x(),
                   mouseEvent->y(),
                   width(),
                   height(),
                   mouseEvent->globalX(),
                   mouseEvent->globalY(),
                   QApplication::desktop()->screenGeometry(this).width(),
                   QApplication::desktop()->screenGeometry(this).height());

  static_cast<MouseListener*>(_scenario)->release(event);
}

void
GLWidget::
mouseMoveEvent(QMouseEvent* mouseEvent) {
  MouseEvent event(static_cast<Button>(mouseEvent->button()),
                   mouseEvent->x(),
                   mouseEvent->y(),
                   width(),
                   height(),
                   mouseEvent->globalX(),
                   mouseEvent->globalY(),
                   QApplication::desktop()->screenGeometry(this).width(),
                   QApplication::desktop()->screenGeometry(this).height());

  static_cast<MouseListener*>(_scenario)->move(event);
}
