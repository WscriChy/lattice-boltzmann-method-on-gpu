#ifndef Scenario_hpp
#define Scenario_hpp

#include "KeyboardListener.hpp"
#include "MouseListener.hpp"
#include "Renderer.hpp"

class Scenario: public KeyboardListener,
                public MouseListener,
                public Renderer {
  // NOTE: The following `using` declarations resolve ambiguity issue.
  using KeyboardListener::press;
  using KeyboardListener::release;

  using MouseListener::press;
  using MouseListener::release;

public:
  virtual
  ~Scenario() = 0;
};

#endif
