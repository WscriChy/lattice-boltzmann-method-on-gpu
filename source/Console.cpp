#include "Console.hpp"
// -----------------------------------------------------------------------------

Console::
Console(QWidget* parent): QTextEdit(parent) {
  setReadOnly(true);
}

void
Console::
print(QString const& string)
{ setText(string); }