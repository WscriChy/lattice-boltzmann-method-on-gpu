#ifndef ScalarField_hpp
#define ScalarField_hpp

#include "Field.hpp"

class ScalarField: public Field {
public:
  ScalarField(GLuint sampleCountX,
              GLuint sampleCountY,
              GLuint sampleCountZ);
};

#endif
