#include "DomainRenderer.hpp"

#include <oglplus/bound/buffer.hpp>

#include <vector>
// -----------------------------------------------------------------------------

using Math::Matrix4x4f;

using oglplus::Buffer;
using oglplus::Capability;
using oglplus::DataType;
using oglplus::FragmentShader;
using oglplus::PrimitiveType;
using oglplus::Program;
using oglplus::Uniform;
using oglplus::VertexArray;
using oglplus::VertexShader;

using std::vector;

namespace {
GLuint const vertexCount = 8;
GLuint const indexCount  = 24;
}

DomainRenderer::
DomainRenderer(GLuint cellCountX,
               GLuint cellCountY,
               GLuint cellCountZ):
  _cellCountX(cellCountX),
  _cellCountY(cellCountY),
  _cellCountZ(cellCountZ) {
  assert(_cellCountX >= 1);
  assert(_cellCountY >= 1);
  assert(_cellCountZ >= 1);

  initializeProgram();
  initializeDomain();
}

void
DomainRenderer::
initializeProgram() {
  VertexShader   vs;
  FragmentShader fs;

  vs.Source(
    "#version 400\n"

    "uniform mat4 _mvp;"

    "in vec3 _position;"

    "void "
    "main() {"
    "  gl_Position = _mvp * vec4(_position, 1);"
    "}").Compile();

  fs.Source(
    "#version 400\n"

    "uniform vec3 _color;"

    "out vec4 f_color;"

    "void "
    "main() {"
    "  f_color = vec4(_color, 1);"
    "}").Compile();

  _program.AttachShader(vs);
  _program.AttachShader(fs);

  _program.Link();

  _program.DetachShader(vs);
  _program.DetachShader(fs);
}

void
DomainRenderer::
initializeDomain() {
  _vertexArray.Bind();
  {
    auto b = Bind(_vertexBuffer, Buffer::Target::Array);

    vector<GLfloat> data;

    data.reserve(3 * vertexCount);

    data.push_back(0.5f);
    data.push_back(0.5f);
    data.push_back(0.5f);

    data.push_back(_cellCountX + 0.5f);
    data.push_back(0.5f);
    data.push_back(0.5f);

    data.push_back(_cellCountX + 0.5f);
    data.push_back(0.5f);
    data.push_back(_cellCountZ + 0.5f);

    data.push_back(0.5f);
    data.push_back(0.5f);
    data.push_back(_cellCountZ + 0.5f);

    data.push_back(0.5f);
    data.push_back(_cellCountY + 0.5f);
    data.push_back(0.5f);

    data.push_back(_cellCountX + 0.5f);
    data.push_back(_cellCountY + 0.5f);
    data.push_back(0.5f);

    data.push_back(_cellCountX + 0.5f);
    data.push_back(_cellCountY + 0.5f);
    data.push_back(_cellCountZ + 0.5f);

    data.push_back(0.5f);
    data.push_back(_cellCountY + 0.5f);
    data.push_back(_cellCountZ + 0.5f);

    b.Data(data);

    (_program | "_position").Setup<GLfloat>(3).Enable();
  }

  {
    auto b = Bind(_indexBuffer, Buffer::Target::ElementArray);

    vector<GLuint> data;

    data.reserve(indexCount);

    data.push_back(0); data.push_back(1);
    data.push_back(1); data.push_back(2);
    data.push_back(2); data.push_back(3);
    data.push_back(3); data.push_back(0);

    data.push_back(4); data.push_back(5);
    data.push_back(5); data.push_back(6);
    data.push_back(6); data.push_back(7);
    data.push_back(7); data.push_back(4);

    data.push_back(0); data.push_back(4);
    data.push_back(1); data.push_back(5);
    data.push_back(2); data.push_back(6);
    data.push_back(3); data.push_back(7);

    b.Data(data);
  }

  VertexArray::Unbind();
}

void
DomainRenderer::
render(Matrix4x4f const& mvp, GLfloat r, GLfloat g, GLfloat b) const {
  auto depthTest = _gl.IsEnabled(Capability::DepthTest);

  _gl.Enable(Capability::DepthTest);

  _program.Use();

  Uniform<GLfloat>(_program, "_mvp").SetMatrix<4, 4>(1, mvp.data());
  Uniform<GLfloat>(_program, "_color").SetVector(r, g, b);

  _vertexArray.Bind();
  _gl.DrawElements(PrimitiveType::Lines, indexCount, DataType::UnsignedInt);
  VertexArray::Unbind();

  Program::UseNone();

  if (!depthTest)
    _gl.Disable(Capability::DepthTest);
}