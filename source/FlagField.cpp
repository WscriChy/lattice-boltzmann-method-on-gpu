#include "FlagField.hpp"

#include <QtCore/QDebug>

#include <oglplus/bound/texture.hpp>
// -----------------------------------------------------------------------------

using oglplus::PixelDataFormat;
using oglplus::PixelDataInternalFormat;
using oglplus::PixelDataType;
using oglplus::TextureMagFilter;
using oglplus::TextureMinFilter;

using std::vector;

FlagField::
FlagField(GLuint sampleCountX,
          GLuint sampleCountY,
          GLuint sampleCountZ): Field(sampleCountX,
                                      sampleCountY,
                                      sampleCountZ,
                                      PixelDataInternalFormat::R8,
                                      PixelDataFormat::Red,
                                      PixelDataType::UnsignedByte,
                                      TextureMinFilter::Nearest,
                                      TextureMagFilter::Nearest) {}