#ifndef Core_GpuCLInitialization_hpp
#define Core_GpuCLInitialization_hpp

#include "CLInitialization.hpp"

class GpuCLInitialization: public CL::CLInitialization {
public:
  GpuCLInitialization();

  GpuCLInitialization(GpuCLInitialization const& other);

  virtual
  ~GpuCLInitialization();

  GpuCLInitialization const&
  operator=(GpuCLInitialization const& other);

  void
  initialize();

  CL::Context const&
  context() const;

  CL::DeviceList const&
  devices() const;

  static CL::CLInitialization::Shared
  createShared() {
    return CL::CLInitialization::Shared(new GpuCLInitialization());
  }

private:
  CL::Context    _context;
  CL::DeviceList _devices;
};   /* ----- end of GpuCLInitialization ----- */

#endif /* ----- end Core_GpuCLInitialization_hpp ----- */
