#include "Field.hpp"

#include <QtCore/QDebug>

#include <oglplus/bound/texture.hpp>

#include <cassert>
// -----------------------------------------------------------------------------

using oglplus::Framebuffer;
using oglplus::FramebufferColorAttachment;
using oglplus::FramebufferTarget;
using oglplus::PixelDataFormat;
using oglplus::PixelDataInternalFormat;
using oglplus::PixelDataType;
using oglplus::Texture;
using oglplus::TextureMagFilter;
using oglplus::TextureMinFilter;
using oglplus::TextureWrap;
using oglplus::TextureWrap;

namespace {
GLuint
upperPowerOfTwo(GLuint x) {
  x--;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  x++;

  return x;
}
}

Field::
Field(GLuint                  sampleCountX,
      GLuint                  sampleCountY,
      GLuint                  sampleCountZ,
      PixelDataInternalFormat internalFormat,
      PixelDataFormat         format,
      PixelDataType           type,
      TextureMinFilter        minFilter,
      TextureMagFilter        magFilter): _sampleCountX(sampleCountX),
                                          _sampleCountY(sampleCountY),
                                          _sampleCountZ(sampleCountZ) {
  assert(_sampleCountX >= 1);
  assert(_sampleCountY >= 1);
  assert(_sampleCountZ >= 1);

  _texelCountX = upperPowerOfTwo(_sampleCountX + 2);
  _texelCountY = upperPowerOfTwo(_sampleCountY + 2);
  _texelCountZ = upperPowerOfTwo(_sampleCountZ + 2);

  {
    auto t = Bind(_texture, Texture::Target::_3D);

    t.BaseLevel(0);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAX_LEVEL, 0);

    t.MinFilter(minFilter);
    t.MagFilter(magFilter);

    t.WrapS(TextureWrap::ClampToEdge);
    t.WrapT(TextureWrap::ClampToEdge);
    t.WrapR(TextureWrap::ClampToEdge);

    t.Image3D(0,
              internalFormat,
              _texelCountX,
              _texelCountY,
              _texelCountZ,
              0,
              format,
              type,
              0);

    Texture::Unbind(Texture::Target::_3D);
  }
}

Field::
~Field() {}

GLuint
Field::
sampleCountX() const
{ return _sampleCountX; }

GLuint
Field::
sampleCountY() const
{ return _sampleCountY; }

GLuint
Field::
sampleCountZ() const
{ return _sampleCountZ; }

GLuint
Field::
texelCountX() const
{ return _texelCountX; }

GLuint
Field::
texelCountY() const
{ return _texelCountY; }

GLuint
Field::
texelCountZ() const
{ return _texelCountZ; }

void
Field::
attach() const
{ _texture.Bind(Texture::Target::_3D); }

void
Field::
attach(GLuint textureUnit) const {
  Texture::Active(textureUnit);
  _texture.Bind(Texture::Target::_3D);
}

void
Field::
detach() const
{ Texture::Unbind(Texture::Target::_3D); }

void
Field::
detach(GLuint textureUnit) const {
  Texture::Active(textureUnit);
  Texture::Unbind(Texture::Target::_3D);
}

GLuint
Field::
expose() const
{ return Expose(_texture).Name(); }