#define INFLOW_SCENARIO

__constant double3 InflowVelocity = (double3)(0.0, 0.0, 0.3);

void
createScenario(
  __global   ClConfig* config,
  __global char*       flagField,
  int                  id,
  int3                 position) {
  if (position.x == 0)
    flagField[id] = NO_SLIP;
  else if (position.x == (config->sizeX - 1))
    flagField[id] = NO_SLIP;
  else if (position.y == 0)
    flagField[id] = NO_SLIP;
  else if (position.y == (config->sizeY - 1))
    flagField[id] = NO_SLIP;
  else if (position.z == 0)
    flagField[id] = INFLOW;
  else if (position.z == (config->sizeZ - 1))
    flagField[id] = OUTFLOW;
  else
    flagField[id] = FLUID;
}
