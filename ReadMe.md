The Real-Time 3D Lattice-Boltzmann Method Using OpenGL and OpenCL
=================================================================

--------------------------------------------------------------------------------

Description
-----------

--------------------------------------------------------------------------------

This project is an open-source implementation of the 3D [Lattice-Boltzmann
method (LBM)][LBM], a popular class of [computational fluid dynamics (CFD)][CFD]
methods for fluid simulation. The implementation takes advantage of [OpenGL][]
to provide high-quality hardware-accelerated visualization and [OpenCL][] to
perform heavy computations associated with the method on [graphics processing
unit (GPU)][GPU]. Although, the algorithm of LBM has many variations, in general
it inherently possesses very good parallelization properties which fit into the
computational model offered by OpenCL specification very well. This project
aims to exploit these properties in order to achieve real-time in both
visualization and computation aspects.

Features
--------

--------------------------------------------------------------------------------

The following types of scenarios are currently supported by the implementation:

  - Lid-driven cavity;
  - Inflow/outflow;
  - Arbitrary geometries (obstacles).

Of course these 3 types can be combined to create arbitrarily complex scenarios.

The following visualization methods are implemented:

  - Density field mapped into color distribution;
  - Velocity field mapped into vector glyph distribution;
  - Particle tracing in the velocity field.

Goals
-----

--------------------------------------------------------------------------------

The following design goals were prioritized during the development:

  - High quality and computational efficiency of visualization methods;
  - High performance and scalability of LBM solver;
  - Efficient OpenGL/OpenCL interoperability with minimum bandwidth;
  - Separation of visualization and computation jobs through utilization of
    multi-threading and their smart management to allow run-time adaptive
    workload balance between OpenGL and OpenCL;
  - Introduction of minimal GUI to allow scenario choosing, parameter
    adjustments, and general control of the application workflow;
  - Object-oriented paradigm domination;
  - Preferably cross-platform application;
  - Utilization of bleeding-edge development tools, libraries, specifications,
    standards, etc.

Implementation
--------------

--------------------------------------------------------------------------------

The application is written in [C++][], more importantly the [C++11][] standard
is employed. As suggested above, the project is divided into 2 main subsystems,
which in turn consist of several important classes and routines listed below:

  - Visualization:

      - `source/DomainRenderer.hpp` - domain boundary visualization;
      - `source/DensityRenderer.hpp` - density field visualization;
      - `source/VelocityRenderer.hpp` - velocity field visualization;
      - `source/ParticleRenderer.hpp` - particle tracing visualization;
      - `source/Camera.hpp` - camera to project 3D world on screen.

  - Computation:

      - `source/LbmSolver.hpp` - low-level mediator for interaction with OpenCL
        LBM solver;
      - `source/LbmWorker.hpp` - thread-safe manager for OpenCL LBM solver;
      - `resource/kernel.cl` - OpenCL LBM solver;
      - `resource/routine.cl` - helper routines for OpenCL LBM solver.

Scenarios are described *procedurally* in OpenCL. Several interesting scenarios
are supplied out-of-the-box:

  - `resource/scenario1.cl` - lid-driven cavity;
  - `resource/scenario2.cl` - channel;
  - `resource/scenario3.cl` - lid-driven cavity with obstacles;
  - `resource/scenario4.cl` - serpentine channel;
  - `resource/scenario5.cl` - serpentine channel.

Creation of new custom scenarios is quite straightforward, simply follow the
same approach as for the default ones.

Limitations
-----------

--------------------------------------------------------------------------------

Although one of the goals is making this project cross-platform, unfortunately,
at the moment, it can only be built for Windows platforms. The reason for this
is that OpenCL context creation and manipulation is platform-dependent. OpenCL
is relatively young technology, and there are still no decent wrappers to make
OpenCL context creation and manipulation cross-platform like it was done for
OpenGL by various libraries. Due to time constraints on the project, the task
for development of such cross-platform interface was given the lowest priority,
and therefore is not implemented yet.

Dependencies
------------

--------------------------------------------------------------------------------

 1. [Qt][], version `4.7.4` or higher.  
    Components:
      - [QtCore][];
      - [QtGui][];
      - [QtOpenGL][].

 2. [Boost][], version `1.50.0` or higher.  
    Components:
      - [Smart Pointers][];
      - [Lexical_Cast][];
      - [Chrono][];
      - [System][].

 3. [Eigen][], version `3.1.3` or higher;
 4. [OGLplus][], version `0.32.0` or higher;
 5. [GLEW][], version `1.9.0` or higher;
 6. [OpenGL][], version `4.0` or higher;
 7. [OpenCL][], version `1.1` exactly;
 8. [OpenCL C++ Bindings][], version `1.1` exactly;
 9. [C++11][] compliant compiler;  
    [GCC][] ([MinGW-w64][] for Windows) or [Clang][] are recommended.

Installation
------------

--------------------------------------------------------------------------------

**NOTE:** The `<...>` notation is used to denote placeholders for your custom
values and paths on your system; feel free to tweak them as you like.

 1. Obtain the source code:

        git clone https://bitbucket.org/Haroogan/lattice-boltzmann-method.git <lbm-dir>

 2. Go to `<lbm-dir>`:

        cd <lbm-dir>

 3. Configure the build system:

        export Boost_INCLUDE_DIR=<boost-include-dir>
        export Boost_LIBRARY_DIR=<boost-library-dir>
        export Eigen_INCLUDE_DIR=<eigen-include-dir>
        export OGLplus_INCLUDE_DIR=<oglplus-include-dir>
        export GLEW_INCLUDE_DIR=<glew-include-dir>
        export GLEW_LIBRARY_DIR=<glew-library-dir>
        export OpenCL_INCLUDE_DIR=<opencl-include-dir>
        export OpenCL_LIBRARY_DIR=<opencl-library-dir>
        cmake/configure.py

    **NOTE:** By default `cmake/configure.py` uses [Ninja][] generator. To
    change that, use the `-generator` option, for example:

        cmake/configure.py -generator="MSYS Makefiles"

    **NOTE:** By default `cmake/configure.py` configures the `Release` variant.
    To change that, use the `-variant` option, for example:

        cmake/configure.py -variant=Debug

 4. Build:

        cmake/build.py

    **NOTE:** By default `cmake/build.py` uses [Ninja][] builder. To change
    that, use the `-builder` option, for example:

        cmake/build.py -builder=make

    **NOTE:** By default `cmake/build.py` builds the `Release` variant. To
    change that, use the `-variant` option, for example:

        cmake/build.py -variant=Debug

 5. Install:

        cmake/install.py

    **NOTE:** By default `cmake/install.py` installs to `<lbm-dir>/install`. To
    change that, specify the `-prefix` option to `cmake/configure.py`, for
    example:

        cmake/configure.py -prefix=<lbm-installation-dir>

    **NOTE:** By default `cmake/install.py` installs the `Release` variant. To
    change that, use the `-variant` option, for example:

        cmake/install.py -variant=Debug

Screenshots
-----------

--------------------------------------------------------------------------------

![Lid-Driven Cavity #1](https://dl.dropbox.com/s/ekkubdpehwzvug7/1.png)

![Lid-Driven Cavity #2](https://dl.dropbox.com/s/0vhmempof8al4rh/2.png)

![Lid-Driven Cavity #3](https://dl.dropbox.com/s/jlwg0zfnpz5ckg9/3.png)

![Lid-Driven Cavity #4](https://dl.dropbox.com/s/p998467sqosqzbw/4.png)

![Lid-Driven Cavity #5](https://dl.dropbox.com/s/zszzmibx7q7xx1p/5.png)

![Lid-Driven Cavity #6](https://dl.dropbox.com/s/mngos6ne7x1o061/6.png)

![Lid-Driven Cavity #7](https://dl.dropbox.com/s/9s2nq7wnt6b2e7i/7.png)

![Serpentine Channel #1](https://dl.dropbox.com/s/odf1el4fh7l1vui/8.png)

![Serpentine Channel #2](https://dl.dropbox.com/s/wqp3l8s020rkjie/9.png)

![Serpentine Channel #3](https://dl.dropbox.com/s/mp8zby79ub1h8lu/10.png)

Copyright
---------

--------------------------------------------------------------------------------

Copyright (C) 2013, Alexander Shukaev  
Copyright (C) 2013, Viacheslav Mikerov

License
-------

--------------------------------------------------------------------------------

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

[LBM]:                 http://en.wikipedia.org/wiki/Lattice_Boltzmann_methods
[CFD]:                 http://en.wikipedia.org/wiki/Computational_fluid_dynamics
[OpenGL]:              http://en.wikipedia.org/wiki/OpenGL
[OpenCL]:              http://en.wikipedia.org/wiki/OpenCL
[GPU]:                 https://en.wikipedia.org/wiki/Graphics_processing_unit
[C++]:                 https://en.wikipedia.org/wiki/C%2B%2B
[C++11]:               https://en.wikipedia.org/wiki/C%2B%2B11
[Qt]:                  https://en.wikipedia.org/wiki/Qt_(framework)
[QtCore]:              http://qt-project.org/doc/qt-4.8/qtcore.html
[QtGui]:               http://qt-project.org/doc/qt-4.8/qtgui.html
[QtOpenGL]:            http://qt-project.org/doc/qt-4.8/qtopengl.html
[Boost]:               http://en.wikipedia.org/wiki/Boost_(C%2B%2B_libraries)
[Smart Pointers]:      http://www.boost.org/doc/libs/1_50_0/libs/smart_ptr/smart_ptr.htm
[Lexical_Cast]:        http://www.boost.org/doc/libs/1_50_0/doc/html/boost_lexical_cast.html
[Chrono]:              http://www.boost.org/doc/libs/1_50_0/doc/html/chrono.html
[System]:              http://www.boost.org/doc/libs/1_50_0/libs/system/doc/index.html
[Eigen]:               http://en.wikipedia.org/wiki/Eigen_(C%2B%2B_library)
[OGLplus]:             http://oglplus.org/
[GLEW]:                http://en.wikipedia.org/wiki/OpenGL_Extension_Wrangler_Library
[OpenCL C++ Bindings]: http://www.khronos.org/registry/cl/api/1.1/cl.hpp
[GCC]:                 https://en.wikipedia.org/wiki/GNU_Compiler_Collection
[Clang]:               http://en.wikipedia.org/wiki/Clang
[MinGW-w64]:           http://en.wikipedia.org/wiki/MinGW#MinGW-w64
[Ninja]:               https://github.com/martine/ninja
